package errmap

import "errors"

type tDefault struct {
	f_do func(error) error
}

type tOnIs struct{ f_on func(error) bool }
type tOnAs[T error] struct{ f_on func(error) bool }

type M struct {
	f_on func(error) bool
	f_do func(error) error
}

func OnIs(target error) tOnIs {
	f := func(err error) bool { return errors.Is(err, target) }
	return tOnIs{f_on: f}
}

func OnAs[T error]() tOnAs[T] {
	f := func(err error) bool {
		var t T
		return errors.As(err, &t)
	}
	return tOnAs[T]{f_on: f}
}

func DefaultErr(err error) tDefault {
	return tDefault{
		f_do: func(_ error) error { return err },
	}
}

func DefaultF(f func(error) error) tDefault {
	return tDefault{f_do: f}
}

func accept_all(_ error) bool {
	return true
}

func (self tDefault) into_m() M {
	return M{
		f_on: accept_all,
		f_do: self.f_do,
	}
}

func OnF(f func(error) bool) tOnIs {
	return tOnIs{f_on: f}
}

func (self tOnIs) DoErr(err error) M {
	return M{
		f_on: self.f_on,
		f_do: func(_ error) error { return err },
	}
}

func (self tOnIs) DoF(f func(error) error) M {
	return M{f_on: self.f_on, f_do: f}
}

func as_is(err error) error {
	return err
}

func (self tOnIs) DoAsIs() M {
	return M{f_on: self.f_on, f_do: as_is}
}

func (self tOnAs[T]) DoErr(err error) M {
	return M{
		f_on: self.f_on,
		f_do: func(_ error) error { return err },
	}
}

func (self tOnAs[T]) DoF(f func(T) error) M {
	wrapped := func(err error) error {
		return f(err.(T))
	}
	return M{f_on: self.f_on, f_do: wrapped}
}

func (self tOnAs[T]) DoAsIs() M {
	return M{f_on: self.f_on, f_do: as_is}
}
