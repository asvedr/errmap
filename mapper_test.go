package errmap_test

import (
	"errors"
	"testing"

	"gitlab.com/asvedr/errmap"
)

var err_val = errors.New("ConstErr")

type err_type struct{ msg string }

func (e err_type) Error() string {
	return "err_type{" + e.msg + "}"
}

var e1 = errors.New("e1")
var e2 = errors.New("e2")
var e3 = errors.New("e3")

func TestMapping(t *testing.T) {
	mappers := []errmap.M{
		errmap.OnIs(err_val).DoErr(e1),
		errmap.OnAs[err_type]().DoErr(e2),
		errmap.OnF(func(x error) bool { return x.Error() == "1" }).DoErr(e3),
	}
	mappings := []errmap.Mapping{
		errmap.WithPanic(mappers...),
		errmap.WithReraise(mappers...),
		errmap.WithDefault(
			errmap.DefaultErr(errors.New("x")),
			mappers...,
		),
	}
	for _, mapping := range mappings {
		err := mapping.Map(nil)
		if err != nil {
			t.Fatal(err)
		}
		err = mapping.Map(err_val)
		if err != e1 {
			t.Fatal(err)
		}
		err = mapping.Map(err_type{msg: "abc"})
		if err != e2 {
			t.Fatal(err)
		}
		err = mapping.Map(errors.New("1"))
		if err != e3 {
			t.Fatal(err)
		}
	}
}

func panics(f func()) bool {
	result := false
	wrapper := func() {
		defer func() {
			if err := recover(); err != nil {
				result = true
			}
		}()
		f()
	}
	wrapper()
	return result
}

func TestPanic(t *testing.T) {
	mapper := errmap.WithPanic(errmap.OnIs(err_val).DoErr(e1))
	p := panics(func() {
		mapper.Map(errors.New("other"))
	})
	if !p {
		t.Fatal("panic expected")
	}
}

func TestDefault(t *testing.T) {
	def_err := errors.New("def_err")
	mapper := errmap.WithDefault(
		errmap.DefaultErr(def_err),
		errmap.OnIs(err_val).DoErr(e1),
	)
	err := mapper.Map(errors.New("other"))
	if err != def_err {
		t.Fatal(def_err)
	}
}

func TestReraise(t *testing.T) {
	mapper := errmap.WithReraise(
		errmap.OnIs(err_val).DoErr(e1),
	)
	src := errors.New("other")
	err := mapper.Map(src)
	if err != src {
		t.Fatal(err)
	}
}

func TestDoF(t *testing.T) {
	f_on_t := func(err err_type) error {
		return errors.New("mapped_as " + err.msg)
	}
	f_on_e := func(err error) error {
		return errors.New("mapped_is " + err.Error())
	}
	mapper := errmap.WithReraise(
		errmap.OnAs[err_type]().DoF(f_on_t),
		errmap.OnIs(e1).DoF(f_on_e),
	)

	err := mapper.Map(err_type{msg: "abc"})
	if err.Error() != "mapped_as abc" {
		t.Fatal(err)
	}

	err = mapper.Map(e1)
	if err.Error() != "mapped_is e1" {
		t.Fatal(err)
	}
}

func TestDoAsIs(t *testing.T) {
	mapper := errmap.WithPanic(
		errmap.OnAs[err_type]().DoAsIs(),
		errmap.OnIs(e1).DoAsIs(),
	)

	err := mapper.Map(err_type{msg: "abc"})
	if err.Error() != "err_type{abc}" {
		t.Fatal(err)
	}

	err = mapper.Map(e1)
	if err != e1 {
		t.Fatal(err)
	}
}
