package errmap

import "fmt"

type Mapping struct {
	mappers            []M
	panic_on_not_found bool
}

func WithPanic(
	mappers ...M,
) Mapping {
	return Mapping{mappers: mappers, panic_on_not_found: true}
}

func WithDefault(
	def tDefault,
	mappers ...M,
) Mapping {
	return Mapping{mappers: append(mappers, def.into_m())}
}

func WithReraise(
	mappers ...M,
) Mapping {
	return Mapping{mappers: mappers, panic_on_not_found: false}
}

func (self Mapping) Map(src error) error {
	if src == nil {
		return nil
	}
	for _, mapper := range self.mappers {
		if mapper.f_on(src) {
			return mapper.f_do(src)
		}
	}
	if self.panic_on_not_found {
		panic(fmt.Sprintf("err %v can not be mapped", src))
	}
	return src
}
